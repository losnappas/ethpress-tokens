<?php
/**
 * Displays options on EthPress' options page.
 *
 * @since 0.1.0
 *
 * @package EthPress_Tokens
 */

namespace losnappas\Ethpress_Tokens\Admin;

defined( 'ABSPATH' ) || die;

/**
 * Static.
 *
 * @since 0.1.0
 */
class Options {

	/**
	 * Adds options page. Attached to hook.
	 *
	 * @since 0.1.0
	 */
	public static function admin_menu() {
		$page = esc_html__( 'EthPress Tokens', 'ethpress_tokens' );
		add_options_page(
			$page,
			$page,
			'manage_options',
			'ethpress_tokens',
			array( __CLASS__, 'create_page' )
		);
	}

	/**
	 * Creates options page.
	 *
	 * @since 0.1.0
	 */
	public static function create_page() {
		?>
		<div class="wrap">
			<h1><?php esc_html_e( 'EthPress Tokens', 'ethpress_tokens' ); ?></h1>
			<h2><?php esc_html_e( 'Users with 0 tokens of specified token will be barred from logging in via EthPress.', 'ethpress_tokens' ); ?></h2>
			<form action="options.php" method="POST">
			<?php
			settings_fields( 'ethpress_tokens' );
			do_settings_sections( 'ethpress_tokens' );
			submit_button();
			?>
			</form>
		</div>
		<?php
	}

	/**
	 * Adds settings for api_url to options page. admin_init hooked.
	 *
	 * @since 0.1.0
	 */
	public static function admin_init() {
		register_setting(
			'ethpress_tokens',
			'ethpress_tokens',
			array( __CLASS__, 'options_validate' )
		);

		add_settings_section(
			'ethpress_tokens_main',
			esc_html__( 'Limit EthPress Logins Based on Token Ownership', 'ethpress_tokens' ),
			array( __CLASS__, 'settings_section' ),
			'ethpress_tokens'
		);
		add_settings_field(
			'ethpress_tokens_options',
			esc_html__( 'Settings', 'ethpress_tokens' ),
			array( __CLASS__, 'settings_field' ),
			'ethpress_tokens',
			'ethpress_tokens_main'
		);
	}

	/**
	 * Outputs section title.
	 *
	 * @since 0.1.0
	 */
	public static function settings_section() {
		?>
		<p>
		<?php
		/* translators: email. */
		echo esc_html_e( 'Need assistance or customizations? Ask via email:', 'ethpress_tokens' );
		?>
		</p>
		<p><a href="mailto:incoming+losnappas-ethpress-tokens-18401660-issue-@incoming.gitlab.com">incoming+losnappas-ethpress-tokens-18401660-issue-@incoming.gitlab.com</a></p>
		<?php
	}

	/**
	 * Outputs options.
	 *
	 * @since 0.1.0
	 */
	public static function settings_field() {
		$options = get_option(
			'ethpress_tokens',
			array(
				'etherscan_api_key' => '',
				'token_clearname'   => '',
				'contract_address'  => '',
			)
		);
		?>
		<h4><?php esc_html_e( 'API key from https://etherscan.io', 'ethpress_tokens' ); ?></h4>
		<input id="ethpress_tokens_etherscan_api_key" name="ethpress_tokens[etherscan_api_key]" placeholder="<?php esc_attr_e( 'API key', 'ethpress_tokens' ); ?>" type="text" class="regular-text" value="<?php echo esc_attr( $options['etherscan_api_key'] ); ?>">
		<p class="description"><?php esc_html_e( 'Without a key, you will get rate limited, and logins will be prevented.', 'ethpress_tokens' ); ?></p>

		<h4><?php esc_html_e( 'Your token\'s contract address', 'ethpress_tokens' ); ?></h4>
		<input id="ethpress_tokens_contract_address" name="ethpress_tokens[contract_address]" placeholder="0x..." type="text" class="regular-text" value="<?php echo esc_attr( $options['contract_address'] ); ?>">

		<h4><?php esc_html_e( 'Name of your token, to be displayed to user', 'ethpress_tokens' ); ?></h4>
		<input id="ethpress_tokens_token_clearname" name="ethpress_tokens[token_clearname]" placeholder="" type="text" class="regular-text" value="<?php echo esc_attr( $options['token_clearname'] ); ?>">

		<?php
	}

	/**
	 * Validates input for api url option.
	 *
	 * @param array $input New options input.
	 *
	 * @since 0.1.0
	 */
	public static function options_validate( $input ) {
		$opts                      = get_option( 'ethpress_tokens', array() );
		$opts['etherscan_api_key'] = trim( sanitize_text_field( $input['etherscan_api_key'] ) );
		$opts['token_clearname']   = trim( sanitize_text_field( $input['token_clearname'] ) );
		$opts['contract_address']  = trim( sanitize_text_field( $input['contract_address'] ) );
		return $opts;
	}


	/**
	 * Adds settings link. Hooked to filter.
	 *
	 * @since 0.1.0
	 *
	 * @param array $links Existing links.
	 */
	public static function plugin_action_links( $links ) {
		$url           = esc_attr(
			esc_url(
				add_query_arg(
					'page',
					'ethpress_tokens',
					get_admin_url() . 'options-general.php'
				)
			)
		);
		$label         = esc_html__( 'Settings', 'ethpress_tokens' );
		$settings_link = "<a href='$url'>$label</a>";

		array_unshift( $links, $settings_link );
		return $links;
	}
}
