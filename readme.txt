=== Ethpress Tokens ===
Contributors: lynn999
Donate link: https://www.crowdrise.com/horn-of-africa/fundraiser/lynn99
Tags: ethpress, login, token, ownership, block
Requires at least: 4.6
Tested up to: 5.5
Stable tag: 1.0.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Premium EthPress add-on. Check user token ownership. Perfect for blocking users that don't own your token.

== Description ==

Add token balance check to [EthPress](https://wordpress.org/plugins/ethpress/).

Using [https://etherscan.io](https://etherscan.io) APIs, this plugin checks users' token ownership on login.

By default, it blocks out users that have 0 of your tokens, and displays an error message in the EthPress login dialog.

Start off by configuring this add-on in your WordPress admin by inputting your,

1. API key for etherscan.io (free, requires registration).
2. Your token's contract address.
3. A name for your token, to be displayed to user upon login.

== Installation ==

Download the zip from https://gitlab.com/losnappas/ethpress-tokens/-/releases .

Install it via WordPress plugin installer by clicking on "Add New" and then on "Upload Plugin".

For development: `git checkout config-files -- . && git reset HEAD`

== Frequently Asked Questions ==

== Screenshots ==

== Changelog ==

= 1.0.0 =

* Initial release 9/2020.

== Upgrade Notice ==
